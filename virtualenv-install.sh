#installation happy base client python for hbase
#References https://happybase.readthedocs.io/en/latest/installation.html
#References https://askubuntu.com/questions/244641/how-to-set-up-and-use-a-virtual-python-environment-in-ubuntu

#set virtualenv
sudo apt-get install python3
sudo apt-get install python-pip
sudo apt-get install python3-pip
pip completion --bash >> ~/.bashrc
pip install --user virtualenvwrapper
echo "export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python" >> ~/.bashrc
echo "source ~/.local/bin/virtualenvwrapper.sh" >> ~/.bashrc

export WORKON_HOME=~/.virtualenvs
mkdir $WORKON_HOME
echo "export WORKON_HOME=$WORKON_HOME" >> ~/.bashrc
echo "export PIP_VIRTUALENV_BASE=$WORKON_HOME" >> ~/.bashrc 
source ~/.bashrc

# sudo find / -name "virtualenv"
sudo ln -s /home/$USER/.local/bin/virtualenv /usr/local/bin/virtualenv

