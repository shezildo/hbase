import happybase

connection = happybase.Connection('localhost', autoconnect=False)

connection.open()

table = connection.table('table_transaction')

# table.put(b'033901099364505', {b'day:tanggal': b'20180101', b'day:amount': b'50000', b'day:remark': b'Payment PLN', b'day:source': b'BRI', b'day:target': b'BCA', b'day:status': b'Debit', b'day:prevbalance': b'1000000', b'day:curbalance': b'950000'})
table.put(b'033901099364505', {b'day:tanggal': b'20180101', b'day:amount': b'100000', b'day:remark': b'Transfer', b'day:source': b'BRI', b'day:target': b'BRI', b'day:status': b'Kredit', b'day:prevbalance': b'250000', b'day:curbalance': b'150000'})        
table.put(b'033901099364505', {b'day:tanggal': b'20180101', b'day:amount': b'150000', b'day:remark': b'DO Pertamina', b'day:source': b'BRI', b'day:target': b'BNI', b'day:status': b'Debit', b'day:prevbalance': b'500000', b'day:curbalance': b'350000'})
