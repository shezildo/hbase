#references https://medium.com/@yzhong.cs/hbase-installation-step-by-step-guide-cb73381a7a4c
#references http://hbase.apache.org/book.html#quickstart

sudo apt-get update
sudo apt-get upgrade

#install java
sudo apt-get install default-jre
sudo apt-get install default-jdk

#check java version
java -version

#set path
export JAVA_HOME=/usr/lib/jvm/java-1.8.0-openjdk-amd64
export PATH=$PATH:$JAVA_HOME/bin
source ~/.bashrc

#download hbase -- per jan 14th stable version
wget http://www-eu.apache.org/dist/hbase/stable/hbase-1.2.6-bin.tar.gz

#extract
tar xzvf hbase-1.2.6-bin.tar.gz
cd hbase-1.2.6
cd conf

#edit hbase-env.sh
sudo sed -i -e 's/# export JAVA_HOME=.*/export JAVA_HOME=\/usr\/lib\/jvm\/java\-1\.8\.0\-openjdk\-amd64/' hbase-env.sh
sudo sed -i -e 's/export HBASE_MASTER_OPTS=.*/# export HBASE_MASTER_OPTS="$HBASE_MASTER_OPTS \-XX:PermSize=128m -XX:MaxPermSize=128m"/' hbase-env.sh
sudo sed -i -e 's/export HBASE_REGIONSERVER_OPTS=.*/# export HBASE_REGIONSERVER_OPTS="$HBASE_REGIONSERVER_OPTS -XX:PermSize=128m -XX:MaxPermSize=128m"/' hbase-env.sh

#edit hbase-site.xml
sudo sed -i -e 's/<configuration>.*/<configuration><property><name>hbase.rootdir<\/name><value>file:\/\/\/home\/'$USER'\/hbase\-1\.2\.6<\/value><\/property><property><name>hbase.zookeper.property.dataDir<\/name><value>file:\/\/\/home\/'$USER'\/zookeper<\/value><\/property>/' hbase-site.xml

#back to hbase-1.2.6 folder
cd ..

#hbase ready
./bin/start-hbase.sh
./bin/hbase shell